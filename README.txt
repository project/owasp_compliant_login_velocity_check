INTRODUCTION
------------

This Drupal module performs OWASP-compliant login velocity check by comparing
geolocation of user's current IP address against geolocation of same
user's IP address during previous login session within time period specified in 
module's settings.

REQUIREMENTS
------------

None.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CUSTOMIZATION
-------------

* To override the default "OWASP login velocity check time interval (minutes)"
or "OWASP login velocity check error message":

you can do it by using this link:
/admin/config/system/owasp-check

* In "OWASP login velocity check time interval (minutes)" field you can change
default time period when login velocity check by comparing geolocation
of user's current IP address against geolocation of same user's IP address
during previous login.

* In "OWASP login velocity check error message" field you can change
default error message about founded issue during login velocity check.
